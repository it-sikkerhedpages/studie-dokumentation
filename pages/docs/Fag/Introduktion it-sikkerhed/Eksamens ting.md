# Eksamen

## Spørgsmålene

1. Netværk med fokus på OSI, TCP/IP modeller og netværksprotokoller samt sikkerhedsniveauer.
- Sikkerhedniveauer -> eks. HTTP vs HTTPS, + vis brugen af de forskellige protokoller i Wireshark
- Screenshots i powerpoint er godkendt

2. Netværk med fokus på trafikmonitorering (sniffing) og skanning.
- Visning af programmer (Wireshark og NMAP), fortæl om hvad man ser samt hvad programmerne gør
- Lav et netværksdiagram om hvorhenne man sniffer

3. Programmering i Python med database og fokus på database sikkerhed.
- Eks. hvad gør de forskellige moduler sikkert (se undervisningen for de forskellige moduler)
- Hvordan sikre man mod eventuelle trusler?
- Svaghed og trusler i databaser

4. Scripting i bash og powershell med fokus på hvordan det kan anvendes i sikkerheds arbejdet
- Firewall regler 
- Automatisering af scripts 
- Dokumentation

5. Programmering i Python med netværk og fokus på sikkerhed i protokoller.
- Vis hvordan man bruger modulerne i python (se de forskellige moduler som er blevet brugt i løbet af undervisningen)
- Eventuelt vis at man kan se hvad der sker i Wireshark, når man kører sit python program, som bruger netværket

## Efter fremlæggelse 
- Spørgsmålene som underviser og censor stiller, kommer til at være omkring det som man har med i sin fremlæggelse