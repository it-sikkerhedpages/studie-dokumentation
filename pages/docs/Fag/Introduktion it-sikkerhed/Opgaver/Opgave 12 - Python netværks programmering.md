# Opgave 12 - Python netværks programmering

1. Fork projektet https://gitlab.com/npes-py-experiments/network-programs til dit eget gitlab namespace
2. Klon projektet til din computer
3. Lav et virtual environment og installer dependencies som beskrevet i readme.md filen https://gitlab.com/npes-py-experiments/network-programs#development-usage

## socket

1. Læs om pythons socket bibliotek https://docs.python.org/3/library/socket.html

2. Læs koden i socket_1.py Hvad gør koden og hvordan bruger den socket biblioteket?
- ![Koden](../../../billeder/Intro-its/Intro-its-socket-1%20kode.png)

- Koden starter med at tage libary'et som en import, og så definere man libary'et som "mysock"
- Når den kører programmet igennem, så starter man ud med at give en url, og så prøver programmet på at åbne siden op i konsolen, igennem et "GET" kald på url'et 

3. Kør koden med default url og observer hvad der sker, du kan også prøve at single steppe filen for at kunne følge med i variabler osv. https://python.land/creating-python-programs/python-in-vscode
- Koden prøver at gå ind på default url'et som er defineret i koden.
- Default url'et giver dog en 400 bad request fejl 

4. Åben wireshark (installer det hvis du ikke allerede har det)
5. Kør koden igen med default url, analyser med wireshark og besvar følgende:
    - Hvad er destinations ip ? 
        - 192.168.0.1
    - Hvilke protokoller benyttes ?
        - DNS
    - Hvilken content-type bruges i http header ?
        - JSON? Er ikke sikker på det
    - Er data krypteret ?
        - Dataen er krypteret, da den kører over https
    - Hvilken iso standard handler dokumentet om ?
        - Jeg kan ikke se indholdet, da url'et giver en 400 bad request

11. Læs socket_2.py og undersøg hvordan linje 38 - 47 kan tælle antal modtagne karakterer

        while True:
            data = mysock.recv(500)
            received.append(data.decode())
            if len(data) < 1:
                break
            for item in received:
                count += len(item)
                if count <= 3000:
                print(item)
                chars_printed = count

- Først oprettes en uendelig løkke med while True:. I hver iteration af løkken modtages data fra en socket-forbindelse ved hjælp af mysock.recv(500) og tilføjes til listen received. Dataene dekodes ved hjælp af data.decode().

- Derefter kontrolleres det, om længden af de modtagne data er mindre end 1 (dvs. om der ikke er mere data at modtage), og hvis det er tilfældet, afbrydes løkken med break.

- Hvis der stadig er mere data at modtage, tælles antallet af modtagne karakterer op ved hjælp af variablen count. Hvis antallet af modtagne karakterer er mindre end eller lig med 3000, udskrives hvert element i listen received ved hjælp af en for-løkke. Variablen chars_printed opdateres også med det samlede antal udskrevne karakterer.

12. Kør socket_2.py og prøv at hente forskellige url's der benytter https, hvad modtager du, hvilken http response får du, hvad betyder de og er de krypteret ?
- Jeg får bad request hvis jeg køre med en https url 
- Jeg går ud fra det er fordi de er krypteret

13. Gentag trin 7 og analyser med wireshark for at finde de samme informationer der

14. Åben socket_3.py, analyser koden og omskriv den så kun http headers udskrives.
- Originalen 

        mysock = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
        mysock.connect(('data.pr4e.org', 80))
        cmd = 'GET http://data.pr4e.org/intro-short.txt HTTP/1.0\r\n\r\n'.encode()
        mysock.send(cmd)

        received =b"" #Stores recieved data in bytes

        while True:
            data = mysock.recv(512)
            if len(data) < 1:
                break
            received += data
        pos = received.find(b"\r\n\r\n") #Look for position of header & blank line
        content = received[pos+4:].decode() 
        print(content)
        mysock.close()

- Den omskrevne kode

        import socket

        mysock = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
        mysock.connect(('data.pr4e.org', 80))
        cmd = 'GET http://data.pr4e.org/intro-short.txt HTTP/1.0\r\n\r\n'.encode()
        mysock.send(cmd)

        received = b""  # Stores received data in bytes

        while True:
            data = mysock.recv(512)
            if len(data) < 1:
                break
            received += data

        pos = received.find(b"\r\n\r\n")  # Look for position of header & blank line
        header = received[:pos+4].decode()
        print(header)
        mysock.close()


## urllib

1. Læs om urllib biblioteket https://docs.python.org/3/library/urllib.html

2. Åbn urllib_1.py og læs koden, hvordan er syntaksen ifht. socket 1-3 programmerne ?
- Syntaksen minder meget om hinanden, dog i stedet for at køre et infinit while-loop, så kører den et for-loop 

3. Hvilket datasvar får du retur hvis du prøver at hente via https, f.eks https://docs.python.org/3/library/urllib.html
- Jeg får en masse HTML, metadata og JavaScript retur 

4. Kør programmet igen med https://docs.python.org/3/library/urllib.html og analyser i wireshark, besvar følgende:
    - Hvad er destinations ip ?
        - 199.232.40.223
    - Hvilke protokoller benyttes ?
        - DNS/TSLv1.3
    - Hvilken content-type bruges i http(s) header ?
        - JSON, er dog ikke sikker
    - Er data krypteret ?
        - Ja, da den kører over https
    - Hvor mange cipher suites tilbydes i Client hello ?
    - 36 
    - Hvilken TLS version og cipher suite bliver valgt i Server Hello ?
    - TLS_AES_128_GCM_SHA256

## BeautifulSoup

1. Læs om BeatifulSoup biblioteket https://beautiful-soup-4.readthedocs.io/en/latest/

2. Hvad er formået med biblioteket ?
- BeautifulSoup er et Python-bibliotek, der bruges til at analysere og hente data fra HTML- og XML-dokumenter. Det giver en nem måde at navigere i dokumentstrukturen og ekstrahere data fra de forskellige tags, attributter og tekstindhold i dokumentet.

- Formålet med BeautifulSoup-biblioteket er at forenkle parsing og scraping af websteder. Ved at bruge BeautifulSoup kan du:

    - Analysere HTML og XML-data og finde de relevante dele af dokumentet, som du vil arbejde med.
    - Navigere gennem dokumentet ved hjælp af forskellige metoder og finde specifikke tags og deres attributter.
    - Ekstrahere tekstindholdet af et tag og manipulere det, hvis det er nødvendigt.
    - Filtrere ud de dele af dokumentet, der ikke er nødvendige, eller som ikke opfylder de ønskede betingelser.
    - Opbygge en database eller en datalog ved at hente data fra flere kilder og organisere dem i en ønsket struktur.
- Alt i alt gør BeautifulSoup det lettere at arbejde med websteder og data, der er indlejret i HTML- og XML-dokumenter, og det giver en praktisk måde at ekstrahere, manipulere og analysere disse data.

3. Åbn urllib_2.py og analyser koden for at finde ud af hvad den gør.
- Dette kodeeksempel importerer nødvendige moduler og værktøjer, herunder Request og urlopen fra urllib.request-modulet, BeautifulSoup fra bs4-modulet og ssl-modulet.

- Herefter opretter det en kontekst, der deaktiverer SSL-certifikatfejl, som kan opstå under web scraping-processen. Herefter beder det brugeren om at indtaste en URL eller anvender en standard-URL (Python-dokumentationssiden), hvis der ikke angives nogen URL.

- Koden opretter også en Request-objekt ved hjælp af brugeragenten 'Mozilla / 5.0' for at undgå HTTP-fejl, der kan opstå under web scraping-processen. Derefter henter koden HTML-dataene fra den angivne URL ved hjælp af urllib.request.urlopen ()-funktionen, som tager anmodningen og konteksten som input, og returnerer en byte-streng af HTML-data.

- Derefter analyserer koden HTML-dataene ved hjælp af BeautifulSoup-biblioteket og parseren 'html.parser'. Det tager HTML-dataene som input og returnerer en BeautifulSoup-objekt, der indeholder de parsede data.

- Koden henter derefter alle 'p'-tags i HTML-dataene ved at kalde soup('p') og gemmer dem i en liste af tag-objekter. Til sidst udskriver koden antallet af 'p'-tags, der er fundet, og URL'en for HTML-dokumentet.

4. Kør programmet

5. Ret i programmet så det tæller et andet HTML tag

        from urllib.request import Request, urlopen
        import urllib.error
        from bs4 import BeautifulSoup
        import ssl

        # Ignore SSL certificate errors
        ctx = ssl.create_default_context()
        ctx.check_hostname = False
        ctx.verify_mode = ssl.CERT_NONE

        url = input('Enter a url: ')
        if url == "":
            url = 'https://docs.python.org'

        # https://medium.com/@raiyanquaium/how-to-web-scrape-using-beautiful-soup-in-python-without-running-into-http-error-403-554875e5abed
        req = Request(url, headers={'User-agent': 'Mozilla/5.0'})

        html = urllib.request.urlopen(req, context=ctx,).read()
        soup = BeautifulSoup(html, 'html.parser')

        # Retrieve all of the anchor tags
        count = 0
        tags = soup('a')
        count = len(tags)

        print(f'found {count} a tags @ {url}')

6. Ret i programmet så det bruger BeatifulSoups .findall() metode

        from urllib.request import Request, urlopen
        import urllib.error
        from bs4 import BeautifulSoup
        import ssl

        # Ignore SSL certificate errors
        ctx = ssl.create_default_context()
        ctx.check_hostname = False
        ctx.verify_mode = ssl.CERT_NONE

        url = input('Enter a url: ')
        if url == "":
            url = 'https://docs.python.org'

        # https://medium.com/@raiyanquaium/how-to-web-scrape-using-beautiful-soup-in-python-without-running-into-http-error-403-554875e5abed
        req = Request(url, headers={'User-agent': 'Mozilla/5.0'})

        html = urllib.request.urlopen(req, context=ctx,).read()
        soup = BeautifulSoup(html, 'html.parser')

        # Find all of the anchor tags
        count = 0
        tags = soup.find_all('a')
        count = len(tags)

        print(f'found {count} a tags @ {url}')


## Feedparser

1. Læs om feedparser biblioteket https://feedparser.readthedocs.io/en/latest/

2. Åbn rssfeed_parse.py og analyser koden for at finde ud af hvad den gør.

- Koden henter RSS feed-data fra 'http://feeds.feedburner.com/TheHackersNews' og behandler hver indgang i RSS-feedet ved at konvertere det til en læsbar streng. Koden bruger BeautifulSoup til at analysere HTML-koden i summary-feltet for hver indgang og fjerne uønskede tegn og kodestykker, før den formaterer en læsbar streng. Derefter skriver koden hver formateret indgang til en Markdown-fil kaldet 'feed.md'.

3. Linje 28, som formatterer modtaget data, har en meget lang sætning.

        rssfeed_parse.py

        f'# {entry.title}\n**_Author: {entry.author}_**  \nPublished:  {entry.published}  \n**_Summary_**  \n{re.sub(r"[^a-zA-Z0-9]", " ", entry.summary).replace(" adsense ", " ").replace(" lt ", " ").replace(" gt ", " ")}  \n  \nLink to full article:  \n[{entry.link}]({entry.link})\n'

- Hvad gør den ? ( du kan prøve at erstatte den med {entry.summary} for at se forskellen)

    - Koden er en linje i scriptet, der opretter en streng med detaljer om hvert feedindlæg i markdown-format. Det inkluderer titel, forfatter, dato og en rensning af opsummeringsteksten for at fjerne specialtegn og reklamer. Det inkluderer også en hyperlink til det fulde indlæg.

4. Find et passende sikkerheds relateret rss feed, på internettet, du vil parse.
- CERTs (Computer Emergency Response Teams) RSS-feed, som indeholder information om de seneste sårbarheder og trusler i cybersikkerhedslandskabet.

5. Omskriv rssfeed_parse.py til at bruge dit valgte feed, ret i formatteringen, så du får et output i markdown filen, der svarer til:


        # Headline/title

        **_Author:_**
        **_Summary:_**
        [link_to_full_article](link_to_full_article)

- 
        f'# {entry.title}\n**_Author: {entry.author}_**  \nPublished:  {entry.published}  \n**_Summary_**  \n{re.sub(r"[^a-zA-Z0-9]", " ", entry.summary).replace(" adsense ", " ").replace(" lt ", " ").replace(" gt ", " ")}  \n  \nLink to full article:  \n[{entry.link}]({entry.link})\n**_Security_**  \n{entry.get("certificate", {}).get("subject", {}).get("CN", "")}\n'



[Cheatsheet](
https://www.markdownguide.org/cheat-sheet)