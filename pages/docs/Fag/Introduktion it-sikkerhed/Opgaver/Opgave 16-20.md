# Opgave 16 - 20
Skud ud til Thobias for guiden
Følg [guiden](https://gitlab.com/tjse28878/networking-basics/-/blob/master/Assignment%2010/ASS_10_Network.pdf)

For at få netværk på hver af maskinerne, skal man gå ind i netværk instillinger, og ændre den fra automatisk dhcp til manual i IPv4. Efter man har gjort det giver man den den korrekte ip-adresse (192.168.10.2 i nr. 1), submasken (24) og default gateway (routerens ip-adresse). Når det er blevet gjort, trykker man på save, og gen etablere forbindensen til netværket på enheden.