# Opgave 21 - 23
Først skal man gå ind på den Puffy, og gøre som man gjorde i opgave 16-20, hvor man tilføjer den nye interface, som skal bruges til kali maskinen. 

Efter man har gjort det, kan vælge hvilke netværksindstillering (vmnet3), som kali maskinen skal have. Når man har gjort det, kan man gå ind i kali maskinen, og definere hvilket ip-adresse den skal have (192.168.12.2/24).

Efter kali maskinen er sat op, skal man gå ind i den ene af Xubuntu maskinerne, og ændre netværket til dhcp, for at man kan få den på nettet, så den kan hoste Damn Vulnerable Web Application (DVWA) til miljøet. 

Efter DVWA er blevet installeret, skal man ændre Xubuntu maskinen tilbage til de netværksindstillinger, som den havde før (vmnet1), og gå ind og ændre ip-adressen tilbage til (192.168.10.2/24).

Tilsidst kan man gå ind i kali maskinen, og gå på den hostede webadresse (192.168.10.2), og begynde at arbejde med selve programmet, som bliver hostet lokalt i miljøet.