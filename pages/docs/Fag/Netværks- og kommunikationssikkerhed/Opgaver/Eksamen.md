# Eksamens emner 

## Bonus
- Husk netværksdiagrammer 
- Brug eksempler fra semesteret af, for at kunne vise praktisk viden og forståelse omkring bestemte emner
- Tænk cia og osi modellerne med i fremlæggelserne
- Opbyg fremlæggelserne op omkring læringsmålene 
- Brug referancer til best pratcies fra Mitre ATK&CK og CIS18
- Brug sund fornuft til at gå alle fremlæggelserne igennem

1. **Krypteret trafik:** Formål, muligheder/begrænsninger, performance, aflytte plaintext/ciphertext/metadata
    - Hvad er formålet med kryptering?
    - Eksempler fra wireshark 
    - HTTP vs HTTPS, vil være et perfekt eksempel
    - Krypterings metoder (TLS)
    - Hvordan påvirker det performance? 
        - Symetrisk kører meget hurtigere 
    - Certifikat, public/private keys 
    - Asymetrisk (opret forbindelser) vs symetrisk (bibeholde forbindelser (bedre performance)) kryptering 
        - symetriske forbindelser
            - Wireless forbindelser
            - koder til zip filer
            - koder til bitlogger
        - Asymerisk kryptering 
            - Public / private key 

2. **Segmentering:** Routable og non-routable subnets, tunneller, firewalls, L2 security, performance/DOS, lateral movement
    - Tag et netværksdiagram, og så lig de forskelle ting/segmenter på diagrammet undervejs 
    - Brug referencer til best practies eks. fra "MITRE ATT&CK"
    - Fordele og ulemper ved Routable og non-routable subnets
    - Tunneller, eks. VPN tunneller, wireguard (se opgaven omkring wireguard)
    - Overvej det fra en truselsaktørs pov, hvor kan man sidde for at få agang til systemerne (eks. det som skete med Mærsk)
    - L2 security, eks arp spoofing (ettercap)
    - Load balancing 

3. **Monitorering:** (funktionel): Beskriv de 3 typer (rød/grøn, grafer og logs) og hvad de bruges til både reaktivt og proaktivt
    - Eksempler fra dashboards (grafer osv)
    - Hvor kommer dataen fra? Meget vigtigt
    - Hvad for nogen ting man skal holde øje med og lave alarmer på, samt er der meget fylde data, falsk positive osv. 
    - Proaktivt
        - Vi ved at der kommer til at ske noget, hvor sikre vi os mod det?
    - Reaktivt 
        - Incedent handling (der er sket noget, hvad gør vi?)
    - Eventuelt start med et netværksdiagram, til at fortælle om et netværk, og på den måde fortæl om hvad og hvor man vil lave monitorering
    - Rød/grøn, handler om alarmer, som kan give et hurtigt overblik over om systemet kører godt eller skidt 
    - Grafer, bruges til at lave fejlfinding 
    - Logs, bruges til at lave fejlfinding samt fodre data til graferne 
    - Hvad for nogen fejl vil vi finde, og hvordan vil vi vise det vi finder?
    - Eventuelt brug Mitre eller cis-kontrol til at finde ud af hvad for nogen ting som man typisk vil lave alarmer på
    - Eksempel på en ting som kunne laves en alarm på (bottleneck)

4. **DNS:** Resolve hostnavne, kryptering, certifikater, DNS og privacy
    - Eksempler fra Wireshark 
    - Et netværksdiagram med client-server og så en masse dns, for at kunne diskutere hvem der ved hvad, hvor omkring rundt på netværket
    - Eksempler fra spoofing 
    - Passiv dns, hvad kan man få ud af informationer?
    - Generelt teori omkring DNS 

5. **Firewalls:** Funktion i netværket, statefull/stateless, perimeter sikkerhed vs. layered security
    - statefull vs stateless
        - Kig på performance, hvor stateless har en meget bedre performance, da den ikke gemmer noget i hukommelsen 
    - vis eksempler på firewall regler, og hvordan den eventuelt arbejde med dem 
    - Brug et netværksdiagram til at overveje hvor mange, og hvor henne firewalls skal være 
    - Host based vs network based firewall
    - Hvilken processere kører på firewallen
    - Defence in depht 

6. **SSL inspection:** Implementation, certifikater, placering i netværket, problemer/muligheder for netværks ejeren og brugere
    - Man in the middle 
    - Metadata
    - Certifikater som bruges til man in the middel i virksomheder, for at virksomheden kan holde øje med medarbejderne
    - Hvordan kan man kigge ned i krypteret trafik 
    - Http vs Https 
    - Teori omkring certifikater (private / public keys) 
    - End to end kryptering 
    - Performance i forhold til kompleksiteten af sikkerheden

7. **IPS og IDS:** Funktion, teknologi, use cases, placering i netværk hostbaseret/netværksbaseret, signatur/anomali detektering, eksempler på regler
    - NTOPNG, til at få logs som man bruge til IDS
    - Eksempler på regler fra Suicarta 
    - Teori omkring IDS/IPS
    - Hostbaseret vs netværksbaseret
    - Signatur vs anomali detektering
    - Overvej at få lagt logs med ind i emnet
    - Eventuelt kobel den sammen med monitorering  
    - Muligvis tilføj noget med hvordan AI, vil komme til at påvirke anomali detektering i fremtiden


## Ting som skal læses op på
- DNS
- Proxy 
