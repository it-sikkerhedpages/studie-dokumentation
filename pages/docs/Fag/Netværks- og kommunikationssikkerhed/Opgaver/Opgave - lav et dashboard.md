# Testing
1. Download a test vm from [here](https://drive.google.com/file/d/1NS1Hc3RrkV3P5gVBHHkNtd6RMFmuRnW8/view?usp=sharing) and install it in vmware workstation
2. Connect it to vmnet111 (or the equivante 192.168.111.0/24 subnet)
3. There is an information page on http://192.168.111.20

Unprivileged user is sysuser:sysuser123 and admins user is root:root123

# Working with grafana
1. Make a block diagram showing the relationship between node_exporter, prometheus and grafana.
2. Add a grafana dashboard that uses the node_exporter values from prometheus
3. Add a grafana dashboard that uses the logs from loki
4. Set up an extra VM with node_exporter and update prometheus to scrape from it

# Løsning

1. Man starter med at installere serveren, som man downloader
2. Efter man har gjort det, booter man den op, og starter en maskine inde på samme netværk
3. Når de er booted op, kan man til gå "192.168.111.20" i en browser 
4. Billed af siden ![192.168.111.20](../../../billeder/Netv%C3%A6rk/192.168.111.20.png)
5. Efter man er der inden, kan man tilgå "192.168.111.20:3000", som er grafana. ![192.168.111.20:3000](../../../billeder/Netv%C3%A6rk/Grafana.png)
6. Når man er inde på Grafana, skal man gå ind og tilføje en data source (Prometheus) ![Prometheus](../../../billeder/Netv%C3%A6rk/Grafana-prometheus.png)
7. Efter det kan man oprette et panel (når man har ventet en time) ![Panels](../../../billeder/Netv%C3%A6rk/Grafana-add-panel.png)
I dette eksempel har jeg brugt "go_memstats_mallocs_total" til grafen
![graf](../../../billeder/Netv%C3%A6rk/Grafana-new-panel.png)
8. Gør det samme med Loki ![Loki](../../../billeder/Netv%C3%A6rk/Grafana-loki.png)
