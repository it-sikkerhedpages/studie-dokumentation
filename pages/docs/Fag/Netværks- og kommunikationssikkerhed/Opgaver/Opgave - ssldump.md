# SSL analysis
- Install ssldump either in Kali or similar Linux box
- Run it from the command line: ssldump -j -ANH -n -i any | jq .
    - For at kunne køre i kali, så skal man først installere "jq" ved at køre "sudo apt-get update" efterfuldt af "sudo apt-get -y install jq"
- Generate some HTTPS traffic, e.g. by browsing
- Evaluate what is shown

    
    - How does ssl dump know the domain name? Could we turn that off?
        - Domænenavnet er typisk inkluderet i Server Name Indication (SNI)-udvidelsen af ​​TLS ClientHello-meddelelsen. SSL-dump læser disse oplysninger fra ClientHello-meddelelsen og bruger dem til at dekryptere SSL/TLS-trafikken

        - Det er ikke muligt at deaktivere indsamlingen af ​​domænenavnet med SSL-dump uden at deaktivere dekryptering af SSL/TLS-trafik helt. Hvis du har brug for at fange SSL/TLS-trafik uden at dekryptere den, kan du overveje at bruge et andet værktøj, såsom Wireshark, som kan fange den krypterede trafik uden at dekryptere den

    - When handshaking, the certificate chain is shown. Should that not be secret?
        - Nej det skal den ikke, da certifikatkæden anses ikke for at være hemmelig, da den er beregnet til at være offentligt synlig og verificerbar.