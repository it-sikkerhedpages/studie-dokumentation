# Instruktioner

- Start wireshark på lokal net/firma net og saml noget data op
- Gem PCAPS
- Se på det: er der ukendte protokoller? ukendte ip adresser? andet interessant?
- Dyk ned i 2-3 streams, og forstå hvad der foregår

Der var nogen protokeller, som jeg ikke havde set før, som eks. "SSDP" og "ARP".

Det meste af dataen som jeg dykkede mere ned i, ved at følge streamen, var krypteret, så på nuværende tidspunkt, fik jeg ikke så meget ud af at kigge på det.