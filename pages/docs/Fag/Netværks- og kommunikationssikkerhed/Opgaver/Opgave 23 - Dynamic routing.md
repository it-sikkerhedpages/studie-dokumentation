# Opgave 23 - Dynamic routing

## Forklar hurtigt hvorfor dynamisk routing er essentiel for internettet
- Dynamisk routing er essentiel for internettet, fordi det tillader netværksenheder at kommunikere og automatisk opdatere deres routing tabeller, så trafikken kan tage den mest effektive rute fra kilden til destinationen

## Lav et netværkseksempel der illustrerer hvorfor vi ønsker at opdatere routing tabeller
![Dynamic-routing](../../../billeder/Netv%C3%A6rk/dynamic%20routing.jpg)

## inkludér hvad der er i de enkelte enheders routing tabeler
- For at kunne se routningstabellen, skal man bruge kommandoen "route print" i windows cmd.

## Bonus: Hvordan passer dette med CIA?
- Konfidentialiteten kan bevares, hvis der bruges sikre protokoller til at overføre routing-oplysninger
- Det hjælper med at opretholde integritet, da det kan sikre, at trafikken går den ønskede vej og undgår uautoriserede ruter
- Dynamisk routing bidrager til at opretholde tilgængelighed, ved at sikre, at trafikken kan fortsætte med at flyde i tilfælde af netværksproblemer