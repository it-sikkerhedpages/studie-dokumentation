# Opgave 26 - IDS/IPS viden 

## Forklar med egne ord hvad et intrusion detection system gør, underbyg med troværdige kilder (links)
- Et intrusion detection system (IDS) er en software- eller hardwareløsning, der er designet til at overvåge et netværk eller en computer for mistænkelig aktivitet og trusler. 
- Formålet med et IDS er at identificere og rapportere potentielle sikkerhedsbrud eller angreb på systemet

[GeeksForGeeks](https://www.geeksforgeeks.org/intrusion-detection-system-ids/)

## Forklar med egne ord hvad et intrusion prevention system gør, underbyg med troværdige kilder (links)
- Et intrusion prevention system (IPS) er en sikkerhedsløsning, der fungerer ved at overvåge netværkstrafik og blokere eller stoppe potentielle angreb eller trusler på en proaktiv måde. 
- IPS er en mere avanceret version af et IDS og kan handle på trusler i realtid, før de når deres mål.

[Palo alto ](https://www.paloaltonetworks.com/cyberpedia/what-is-an-intrusion-prevention-system-ips)

## Forklar forskellen på et Netværks IDS/IPS (NIDS) og et host IDS/IPS (HIDS)
- Forskellen mellem NIDS/NIPS og HIDS/HIPS er altså, at NIDS/NIPS overvåger hele netværket og identificerer angreb, der er rettet mod netværket, mens HIDS/HIPS fokuserer på at overvåge en enkelt enhed og identificere angreb, der er rettet mod denne specifikke enhed.

## Forklar med egne ord hvad signatur baseret detektering er, underbyg med troværdige kilder (links)
- Signatur-baseret detektering er en metode til at identificere kendte skadelige aktiviteter baseret på deres unikke digitale signaturer. 
- En signatur er et unikt mønster eller en kode, der identificerer en bestemt type skadelig software eller angrebsvektor.

## Forklar med egne ord hvad anomali baseret detektering er, underbyg med troværdige kilder (links)
- Anomali-baseret detektering er en metode til at identificere mistænkelige aktiviteter eller adfærd, der afviger fra det normale adfærdsmønster i et system eller netværk. 
- Det kan hjælpe med at identificere ukendte trusler, som ikke har en kendt signatur eller et kendt mønster.

## Forklar mulige problemer med hhv. signatur og anomali baseret detektering
## Problemer med signatur-baseret detektering:
- Signatur-baseret detektering er afhængig af at have kendte signaturer for skadelig software. Hvis en trussel ikke har en kendt signatur, vil den ikke blive opdaget.
- Signatur-baseret detektering kan også give falske positiver, når legitime aktiviteter eller software opfattes som skadelige på grund af en signatur, der ligner en kendt trussel.
- Signatur-baseret detektering kan være ineffektiv mod avancerede og skjulte trusler, da deres signaturer kan være ændret eller skjult.

## Problemer med anomali-baseret detektering:

- Anomali-baseret detektering kan producere mange falske positiver, når legitime aktiviteter eller adfærd afviger fra det normale adfærdsmønster.
- Anomali-baseret detektering kan også misse nogle trusler, der er i stand til at tilpasse sig systemets adfærdsmønster eller efterligne legitime adfærd.
- Anomali-baseret detektering kan kræve en længere indkøringsperiode for at oprette et normalt adfærdsmønster, og det kan være vanskeligt at opdage langsomme og gradvise ændringer i adfærdsmønstret over tid.