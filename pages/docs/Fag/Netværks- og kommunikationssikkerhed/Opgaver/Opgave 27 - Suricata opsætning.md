# Opgave 27 - Suricata opsætning

1. Hent og åbn en [basis debian vm](https://drive.google.com/file/d/17rhq9i1fUdBzMo-S53T_Km0YRlcT1S97/view?usp=sharing) uden desktop installeret.
- Der er 2 brugere. user:user1234 og root:root1234
- ssh server er installeret så du kan tilgå maskinen fra en anden VM, f.eks din Kali
- Network adapter skal ændres til det vmnet du ønsker at anvende, du skal have internetforbindelse (ie. vmnet8)

2. Tegn et netværks diagram over netværket

3. Installer Suricata
- sudo apt-get install suricata [kilde](https://suricata.readthedocs.io/en/latest/install.html#debian)
- kontroller at suricata er installeret: suricata -V

5. Konfigurer Suricata med din foretrukne tekst editor. Konfigurations filen er i "/etc/suricata/suricata.yaml"

-  Under "default-rule-path" kan du angive en mappe hvor suricata henter regler, du kan angive flere lokationer. Ret så det ligner nedenstående:

        default-rule-path: /etc/suricata/rules

        rule-files:
            - local.rules

5. Opret filen "local.rules" i mappen "/etc/suricata/rules" og tilføj denne linie (regel) i "local.rules" filen:
        
        alert icmp any any -> any any (msg:"ping/icmp detected"; sid:5000000;)

6. Test at suricata virker og accepterer din nye regel (det vil sige ingen errors i output), brug kommandoen "sudo suricata -T"

7. Lav en mappe der hedder "pcap-files" i user home og gem filen [ping.pcapng](https://ucl-pba-its.gitlab.io/23f-its-nwsec/pcap/ping.pcapng) i mappen
![filen](../../../billeder/Netv%C3%A6rk/opgave%2027%20-%20suricate%20insert%20ping.pcap%20file.png)

8. Kør "pcapng" i suricata med kommandoen "sudo suricata -v -r ping.pcapng" og kig på output, hvad ser du?
![pcap](../../../billeder/Netv%C3%A6rk/Opgave%2027%20-%20suricata%20run%20pcap%20file%20.png)

9. Undersøg indholdet i de 4 nye filer som suricata har genereret i pcap-files mappen, hvad er indholdet og hvad kan det bruges til?
Forstå og dokumenter det.

## Fast.log 
- Denne logfil kan være relateret til en webserver eller applikationsserver kaldet "FAST". Loggen kan indeholde oplysninger om, hvilke sider eller API-kald der er blevet anmodet om, og hvilke fejl der er opstået.

## Eve.Json
- Dette kunne være en logfil fra en intrusion detection system (IDS) eller en intrusion prevention system (IPS) kaldet "EVE". Filformatet angiver, at loggen indeholder data i JSON-format, som er en almindelig måde at strukturere data på.

## Stats.log
- Denne logfil kan indeholde statistikker om forskellige aspekter af et system eller en applikation. Statistikkerne kan omfatte antallet af anmodninger, svarhastigheder, CPU-brug eller andre relevante målinger.

## suricata.log 
- Dette kan også være en logfil fra et IDS eller IPS-program, kaldet "Suricata". Suricata er kendt for at være et open source-system til trusselsdetektion og -forebyggelse, der kan registrere og blokere angreb i realtid.