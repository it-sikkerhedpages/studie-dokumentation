# Opgave 3 - Fejlfinding 
1. Jeg havde et problem, hvor jeg ikke kunne skrive i min virtuelle maskine. 
2. Problemet gjorde så jeg ikke kunne indtaste noget i min vm. Derfor kunne jeg ikke udføre det arbejde som jeg skulle lave. 
3. Den virtuelle maskine kunne ikke indlæse min indput korrekt.
4. Der blev ikke vist noget på skærmen, efter jeg prøvede at lave et tekst indput.
5. Den virtuelle maskine skulle opdateres, og derefter skulle den genstarte, for at kunne læse det indput som jeg gav den.  
6. Tjek efter om maskinen er "up-to-date", med de seneste opdateringer.
7. Opdater maskinen.
- Bonus - Slå tingen op på nettet, for at se efter om andre har haft det samme problem før, og eventuelt er nået frem til en løsning.