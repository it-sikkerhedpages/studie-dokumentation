## Opgave 4 - Specs
1. Find listen fra ww06, hvor lavede vi en liste over hardware og deres plads i OSI modellen.

2. Udvælg 2-3 forskellige enheder fra listen
- repeater 
- accespoint 
- router 
- modem

3. Find specifikationerne for enheden
- Enheden som jeg har taget udgangspunkt i, er det link, som er blevet lagt ud sammen med opgaverne. [Juniper](https://www.juniper.net/us/en/products/security/srx-series/srx300-line-services-gateways-branch-datasheet.html)
- Et stykke nede på siden, kan man se en tabel over spesifikationerne, samt se samligninger med andre modeller. 
- Man kan se en liste over hvad for nogen hardwarebegrænsninger, eks. antal gb-porte, hvor meget ram og haddisk som den kan have, samt den fysiske størrelse som enheden har.

- En anden enhed er [Asus router](https://www.asus.com/dk/networking-iot-servers/wifi-routers/asus-wifi-routers/rt-ac66u-b1/) 

- Dens specifikationer er 
    - Dataforbindelsesprotokol - Ethernet, Fast Ethernet, Gigabit Ethernet, IEEE 802.11b, IEEE 802.11a, IEEE 802.11g, IEEE 802.11n, IEEE 802.11ac
    - Interface	- WAN: 1 x 1000Base-T - RJ-45
    LAN: 4 x 10Base-T/100Base-TX/1000Base-T - RJ-45
    USB 2.0: 2 x 4 pin USB Type A
    - RAM -	256 MB
    - Flashhukommelse - 128 MB


4. Hvad bliver listet som begrænsninger? Synes du det er høje tal? lave tal? 
- Nogle af begrænsningerne som enhenden har er det totale antal af porte, som er tilgængelig. Man kan se at enheden har 8x1GbE porte, hvor 6 af dem er RJ-45 porte. Hvilket der er en "normal" størrelse til mindre netværk, hvor der ikke er brug for så mange kan tilgå dem. 
- Hvis man derimod skal have mange som tilgår enheden igennem de fysiske porte, er det et lille antal af porte, som er tilgængelig på denne enhed. 

- Hvis man kigger på enhed 2, så kan man se at den har meget mindre ram end den første enhed. Derved kan man se at den næsten udelukkende skal bliver brugt hjemmenetværk.