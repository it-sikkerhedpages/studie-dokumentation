# Opgave 5 - internet speeds
1. Gå til [Speedtest](https://speedtest.net/)
2. Hvad er hastigheden?
- Download = 52.79 Mb/S, Upload = 71,01 Mb/S på EDUROAM
- Download = 232,28 MB/S, Upload = 25,53 Mb/S der hjemme
3. Best guess: hvad er bottleneck, og hvorfor? 
- Bottleneck er når man rammer det maksimale en enhed kan klare, og når der ikke er nok resourcer til at kunne skabe en stabil forbindelse.
- Man rammer bottelneck når der er flere enheder som prøver at tilgå eks. et accespoint end accespointet kan kapere. 