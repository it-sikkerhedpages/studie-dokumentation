# Opgave 7 - IPAM 
2. Research hvad netbox er, lav en liste over hvad det kan og hvad det ikke kan.
- Netbox er et værktøj, som man kan bruge til at have en single source of true, hvor man har en oversigt over netværket. 

- Liste med ting som netbox kan 
        [Link til liste](https://docs.netbox.dev/en/stable/introduction/)

- ting de ikke kan 
    - Network monitoring
    - DNS server
    - RADIUS server
    - Configuration management
    - Facilities management

3. Set netbox VM op
- Det første man gør er at downloade netbox.ova filen, og tilføj den til vmware.
- Når den kører, går man ind på ip-adressen i en browser, hvor man logger ind med "netbox:netbox". 
- Efter man er logget ind, kan man begynde at udfylde tingen, ud fra [listen](https://docs.netbox.dev/en/stable/getting-started/planning/)

4. Når det er gjort kan man se en oversigt og overvåge når der sker ændringer