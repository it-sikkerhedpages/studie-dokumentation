# Creating subnets & Importing router
1. Start med at downloade [Kali live](https://moozer.gitlab.io/course-networking-security/Bonus/kali_on_vmware/)

2. Opret en ny virtuel maskine, med den downloadet kali.os fil. (Vælg Debian 11.x 64bit), og navngiv den til client-1

3. Gør det samme med en ny virtuel maskine, og kald den client-2

4. Når maskiken er oprettet, går man ind i settings på begge maskiner, og vælger Network Adapter => derefter vælges "Custom: Specific virtual network", om man vælger VMnet1(Host only) dette gøres på begge maskiner. Går ind i dem og bruger "ip addr" for at find ip-adresserne. Når man kender ip-adresserne, kan man bruge ping funktionen, for at se efter om der er en forbindelse. 

5. Herefter opretter vi et nyt subnet værk ved navn vmnet111 Det gøres ved at man trykker på edit i vmware vælger virtual network edioter. Her opretter jeg så 2 nyt subnets en vmnet111 og vmnet112.

6. Download [router-ova](https://moozer.gitlab.io/course-networking-basisc/05_more_routing/setup/#importing-router-1h)

7. Når routeren er downloadet, importer den til vmware

8. Når det er gjort, højre click på routeren, gå ind i settings og så hvilke netværk den var connectede til. Alle 3, vmnet8, vmnet111 og vmnet112

9. Til sidst, loggede jeg ind på min client1 og pingede det nye subnet (vmnet111) og modsadt for client2.