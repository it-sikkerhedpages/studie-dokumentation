# Noter

## Læringsmål 

### Viden
  #### Generelle governance principper / sikkerhedsprocedurer
-   Ramme værker, for at skabe noget regler for at kunne sikre sit system, (alt papirarbejdet)

   #### Væsentlige forensic processer
   - Måder man kan lave data analyser og efterforsking, for at finde frem til eventuelle begået kriminilitet

   #### Relevante it-trusler
   - Forskellige former for it krimilitet, eks. Malware, CaaS osv. 

   #### Relevante sikkerhedsprincipper til systemsikkerhed
   -    Måder at kunne sikre sit system

   #### OS roller ift. sikkerhedsovervejelser
- Forskellige former for os systemer + overvejelser, også forskellige roller som skal have adgang til at kunne se de forskellige ting.

#### Sikkerhedsadministration i DBMS
- Administrationen af database systemer

### Færdigheder 
#### Udnytte modforanstaltninger til sikring af systemer
- Måder at kunne sikre sit system på, eks. igennem en firewall 

#### Følge et benchmark til at sikre opsætning af enhederne
- Kunne følge en guide til opsætning af enheder

#### Implementere systematisk logning og monitering af enheder
- Intrusion detection, prevention detection. (WireShark)

#### Analysere logs for incidents og følge et revisionsspor
- Man skal se efter om der eventuelt været nogen inceidents, og kunne fortælle om hvordan det eventuelt kunne være sket

#### Kan genoprette systemer efter en hændelse.
- Håndteringen af genoprettelse af systemer 

### Kompetancer
#### håndtere enheder på command line-niveau
- Kunne styre enheder fra en commando linje, unden at se en gui

#### håndtere værktøjer til at identificere og fjerne/afbøde forskellige typer af endpoint trusler
- Man kan bruge forskellige værktøjer som eks. wireshark og burpsuite til at finde frem til forskellige typer af trusler 

#### Håndtere udvælgelse, anvendelse og implementering af praktiske mekanismer til at forhindre, detektere og reagere over for specifikke it-sikkerhedsmæssige hændelser.
- Finde frem til den bedste løsning på problemerne
#### håndtere relevante krypteringstiltag
- Kunne håndtere hvad for nogen data som skal krypteres, eventuelt i en database.


## Installation af linux ubuntu server
- Start med at download ubuntu server fra https://ubuntu.com/download/server
- Når den er download følg guiden fra https://thesecmaster.com/install-ubuntu-linux-on-vmware-workstation/

- Når installationen er kørt, skal man køre "sudo apt update" for at opdatere, og så køre "sudo apt upgrade" for at opgradere systemet til den seneste version. 

- Efter er det kørt igennem, er installationen færdig, og man er klar til at kunne bruge serveren.