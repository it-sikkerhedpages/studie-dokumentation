# Primærer log file

- 'cd /var/log' 
- cat syslog
- Det viser log filen, med en oversigt over det meste som sker i systemet.
- Den bruger dansk tidszone (UTC +1 i vintertid, +2 i sommertid)

# Authentication log 
- Brug 'tail -f auth.log' til at se filen
- Man kan se hvis nogen har prøvet at logge ind
- Hvis man fejler koden, kan man se at en bruger har prøvet at logge ind på root brugeren.