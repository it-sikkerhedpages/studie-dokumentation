# Opgave 17 - Spørgsmål om logging i Linux

1. Hvad hedder den rsyslog file som indeholder alle begivenheder som relaterer til autentificering?
- Typisk vil den ligge i "/var/log/auth.log" 

2. Hvilken log file indeholder oplysninger om bruger der er logget ind på nuværende tidspunkt?
- Typisk vil den ligge i "/var/log/wtmp" 

3. Hvilken log system eksisterer på næsten alle moderne Linux systemer?
- Det vil være rsyslog 