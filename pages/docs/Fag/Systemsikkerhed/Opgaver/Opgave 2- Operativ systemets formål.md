# Opgave 2 - Operativ systemets formål

1. Hvert team medlem skal med udgangspunkt i forberedelsen til idag, reflektere over formålet med operativ systemer og deres kontekst ift. IT-sikkerhed.
- Operativ systemet styrre alt fra hukommelse, processer, al software og hardware i computeren. Det er operativ systemet, som sikre at der kan køres flere ting på en gang, uden at systemet brænder sammen. 
- Indenfor sikkerhed, er operativsystemet meget vigtigt, da det er hjernen bag hele computeren, og hvis en hacker får adgang til operativ systemet, så kan han styre omtræng hele computeren.