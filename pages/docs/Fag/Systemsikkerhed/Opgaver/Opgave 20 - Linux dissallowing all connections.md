# Opgave 20 - Linux dissallowing all connections

1. Udskriv regel kæden, og noter dig hvordan den ser ud. (Regel kæden er tidligere blevet udskrevet i opgave 19, trin 4)
![Firewall](../../../billeder/System-sikkerhed/Opgave%2019%20-%20firewall%20regler.png)

2. Lav en regel i slutningen af regelkæden som dropper alt trafik med kommandoen "sudo iptables -A INPUT -j DROP"

3. Udskriv regel kæden igen, og noter forskellen fra trin 1.
![Firewall](../../../billeder/System-sikkerhed/opgave%2020%20firewall%20regel%20drop.png)

- Forskelle på de to kæder, er at "DROP" er blevet tilføjet under "INPUT", ellers er de to kæder ens 