# Opgave 24 - Linux allow SSH

1. Tillad SSH forbindelse med kommandoen "sudo iptables -A INPUT -p tcp --dport ssh -j ACCEPT"

2. Istedet for at skrive SSH er der også en specifik port, som der typisk vil kunne åbnes op for i stedet, hvilken en port er det?
- Proten som vil være åbnet op er TCP-porten (Port 22 (Default SSH port))