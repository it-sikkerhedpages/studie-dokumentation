# Opgave 25 - Linux allow specific ip

1. Lav en regel der tillader modtagelse af beskeder fra en ip adresse med kommandoen "sudo iptables -A INPUT -p tcp -s <Source IP> --dport 22 -m conntrack --ctstate NEW -j ACCEPT"

2. Lav en regel der tillader afsendelse af beskeder til en specifik ip adresse med kommandoen "sudo iptables -A OUTPUT -p tcp --dport 53 -m conntrack --ctstate NEW -j ACCEPT"