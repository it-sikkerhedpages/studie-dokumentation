# Opgave 26 - Installer auditd

1. Ubuntu har som udgangspunkt ikke audit daemon installeret. Installer audit daemon med kommandoen "sudo apt install auditd"

2. Verificer at auditd er aktiv med kommandoen "systemctl status auditd."
    - ![systemctl](../../../billeder/System-sikkerhed/systemctl%20status%20audit%20opg%2026.png)

3. brug værktøjet auditctl til at udskrive nuværende regler med kommandoen "auditctl -l"
    - ![auditctl](../../../billeder/System-sikkerhed/sudo%20auditctl%20-l%20opg%2026.png)

4. Udskriv log filen som findes i "/var/log/audit/audit.log"
    - ![udskrivning](../../../billeder/System-sikkerhed/udskrivning%20af%20log%20opg%2026.png)

Hvis loggen virker uoverskuelig, har du det tilfælles med næsten resten af verden. Derfor bruger man typisk to værktøjer benævnt ausearch og aureport til at udlæse konkrette begivenheder. Begge værktøjer arbejdes der med i de kommende opgaver.

At arbejde direkte med auditd er en smule primitivt ift. til mange andre værktøjer. Men det giver en forståelse for hvordan der arbejdes med audit på (næsten) laveste nivevau i Linux. Mange større og mere intuitive audit systemer (F.eks. SIEM systemer) benytter sig også af auditd, eller kan benytte sig auditd loggen.