# Opgave 27 - audit en file for ændringer

## Tilføj audit regel med auditctl

1. Opret en audit regel for hver gang der skrives eller ændres på filen "/etc/passwd" med kommandoen "sudo auditctl -w /etc/passwd -p wa -k user_change"

Muligheden "-w" står for "where" og referer til den file hvor audit reglen skal gælde. "-p" står for "permission" og fortæller hvilken rettigheder der skal overvåges. I eksemplet står der "wa" hvilket står for attribute og write. Hvis filens metadata ændres, eller der skrives til filen, udløser det en begivenhed og der bliver oprettet et audit log

2. udskriv en rapport over alle hændelser der er logget med kommandoen "aureport -i -k | grep user_change"

Muligheden "-i" står for intepret, og betyder at numerisk entiter såsom bruger id bliver fortolket som brugernavn. "-k" står for key, hvilket betyder at regel der udløst audit loggen skal vises

- ![aureport](../../../billeder/System-sikkerhed/aureport%20opg%2027.png)

3. tilføj noget text i bunden af filen fra trin 1.

- ![ekstra](../../../billeder/System-sikkerhed/ekstra%20tekst%20opg%2027.png)

4. Udfør trin 2 igen, og bekræft at der nu er kommet en ny række i rapporten
Der kommer en del nye rækker

5. Brug kommandoen "sudo ausearch -i -k user_change". Her skulle du gerne kunne finde en log som ligner den nedstående.
- ![ekstra](../../../billeder/System-sikkerhed/ausearch%20opg%2027.png)

## Tilføj audit regel med audit regel konfigurations file

1. Åben filen "/etc/audit/audit.rules"
- ![Åben](../../../billeder/System-sikkerhed/%C3%A5ben%20filen%20opg%2027.png)

2. Opret en ny file som indeholder reglen, med kommandoen "sh -c "auditctl -l > /etc/audit/rules.d/custom.rules"
For at kommandoen virker skal reglen der blev lavet trin 1 forrige afsnit stadig være gældende, kontroller evt. med "auditctl -l" inden trin 2 udføres

3. Genstart auditd med kommandoen "systemctl restart auditd"

4. Udskriv indholdet af "sudo cat /etc/audit/audit.rules"

5. Slet filen "/etc/audit/rules.d/custom.rules" efter øvelsen
- ![Åben](../../../billeder/System-sikkerhed/%C3%A5ben%20filen%20opg%2027.png)

## Audit alle file ændringer
1. Eksekver kommandoen "aureport -f"
- ![Åben](../../../billeder/System-sikkerhed/aureport%20-f%20opg%2027.png)

2. Ændre kommandoen fra trin 1 således den skriver brugernavn i stedet for bruger id
- ![Åben](../../../billeder/System-sikkerhed/aureport%20-f%20-i%20opg%2027.png) 
- For at kunne udskrive brugernavn skal man bruge "-i", så kommandoen vil være "aureport -f -i"