# Opgave 29 - audit OS api'et for specifikke system kald

1. Tilføj reglen for "sudo auditctl -a always,exit -F arch=b64 -S kill -F key=kill_rule"

2. start en baggrunds proces med kommandoen "sleep 600 &"
![sleep](../../../billeder/System-sikkerhed/sleep%20opg%2029.png)

3. find proces id'et med "ps aux" på sleep processen

4. dræb processen med kommandoen "kill <proces id>"
![kill](../../../billeder/System-sikkerhed/kill%20opg%2029.png)

5. Eksekver kommandoen "aureport -i -k | grep kill_rule" og verificer at der er lavet nye rækker.
![aureoirt](../../../billeder/System-sikkerhed/aureport%20opg%2029.png)

Formodentlig en del nye rækker, auditd har ofte sit eget liv