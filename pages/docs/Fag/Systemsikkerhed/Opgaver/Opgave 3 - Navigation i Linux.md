# Opgave 3 - Navigation i Linux filesystem med bash
1. Udfør kommandoen, observer resultatet og noter
- pwd 
    - pwd udskriver navnet på det directory som man befinder sig i 
- cd
    - cd er kommandoen for change directory, så er den måde man navigere rundt i mapper i linux 
    - "cd .." gør så man hopper et directory tilbage
    - "cd  "gør så man hopper tilbage til root 
- ls
    - ls er kommandoen for at få vist en liste af de filer og mapper som ligger i directory som man befinder sig i
- touch
    - touch kommandoen modificere timestamp for en fil 
- mkdir
    - mkdir kommandoen bruges til at oprette nye directories 
    - Kommandoen skrives som "mkdir [directory name]"
