# Opgave 30 - checksum fra file

1. Installer "hashalot" med "apt".
- "sudo apt install hashalot"

2. Opret en file som indeholder teksten "Hej med dig".
- sudo nano hej.txt

3. Lav en checksum af filen med kommandoen "sha256sum <path to file>".
![sha](../../../billeder/System-sikkerhed/sha%20v1%20opg%2030.png)

4. Lav endnu en checksum af filen, og verificer at check summen er den samme.
- ![sha](../../../billeder/System-sikkerhed/sha%20v1%20opg%2030.png)

5. Tilføj et "f" til teksten i filen.

6. Lav igen en checksum af filen.

7. Verificer at checksum nu er en helt anden.

- ![check](../../../billeder/System-sikkerhed/sha%20v2%20opg%2030.png)
- Det er en anden checksum når man ændre i filen
- Hvis man ændre den tilbage til det som den var før, så kommer checksummen til at være den samme som tidligere