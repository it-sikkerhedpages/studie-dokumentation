# Opgave 4 - Individuelt undersøg og noter i linux cheats sheet hvad det er for nogle filer som er tiltænkt hver enkelt directory

1. Undersøg og noter

- /
    - Hele systemsets root 
- |_bin 
    - bin indehlder commands der kan bruges både af en adminnistrator og en user men ikke har brug for andre system til at blive kørt.
-  |_boot
    - indeholder filer som bruges når maskinen starter op
- |_dev
    Dev indeholder "device filer"??
- |_etc
    - Her ligger vores connfuguration filer, Fx. log filer og filer som skal bruges når systemmet starter
- |_home
    - Her ligger hver bruges individuelle filer
 - |_media
    - Her vil det kunne ses hvis vi fx satte en cd i vores computer, så ville der blive lavet et ny directory.
- |_lib
    - Indeholder informationer som bliver brugt af programmer.
- |_mnt
    - Bruges til at opbevarer data fra floppy disks eller cd'er
- |_misc
    - misc ?
|_proc
    - Virtuelt filsystem, der leverer proces- og kerneinformation som filer. I Linux svarer det til et procfs-mount. Generelt automatisk
- |_opt
    - Her ligger alle addons/ tilføjelser til et downloadedet program som ikke kommed med standart
- |_sbin
    - indeholder system admin filer.
- |_root
    - Home directory for the root user
- |_tmp
    - Her ligger der midlertidige filer som fx hvilke sider vi har besøgt. Slettes normalt når maskinen genstarter
- |_usr
    - Indeholder read only user data
- |_var
    - Her opbevarer systemmet filer som ændre sig imens systemet kør.