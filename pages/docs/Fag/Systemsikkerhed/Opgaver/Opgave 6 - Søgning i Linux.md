# 6 - Søgning i Linux file strukturen
1. Udfør kommandoen, observer resultatet og noter
- find 
    - find kommandoen gør så man kan søge efter et filer, directories osv. 
    - find kommandoen har mange forskellige tilføjelse som man kan gøre brug af når man søger. Man kan eks. søge efter en fil med et bestemt navn "find . -name tecmint.txt". Man kan også putte parametere på som eks. "find / -perm /u=r", som finder filer som er read-only. 

