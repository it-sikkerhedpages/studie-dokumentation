# Opgave 9 - Bruger konti i Linux 
- Opret en ny bruger 
    - "sudo useradd Mandalorian".
    - Hvor "Mandalorian" er det brugernavn, som man vil give den nye bruger.

- Tildel et password til den nye bruger 
    - "sudo passwd Mandalorian"
    - Når man har gjort det skal man angive det nye password 2 gange, hvor den så gemmer det.


- Ændring af eksisterende bruger konto opsætning
    - "sudo usermod -e 2023-06-23 Mandalorian". 

- Ændring af brugerens hjemme directory
    - "mkdir mandoFiles".
    - Når det er gjort, skal man gå ind i directoriet, og bruge "sudo usermod -m /home/mandoFiles Mandalorian", for at gøre den til home directory på Mandalorian brugeren.

- Oprettelse af filer
    - Først brug "sudo chown Mandalorian /home/mandoFiles" inde i det nye directory
    - Brug "touch file1.txt file2.txt file3.txt" til at oprette filerne

- Gentag stepsne for en ny bruger (Ivan)
    - sudo useradd Ivan
    - sudo passwd Ivan
        - i
    - sudo chown Ivan /home/mandoFiles
    - touch file4.txt file5.txt file6.txt

- Se rettigheder
    - ls -l file1.txt
    - ls -l file4.txt

- Filer tilknytte bruger systemet - opbevaring af bruger kontoer
    - ls passwd -al
    - nano passwd, til at se oplysninger

- Filer tilknyttet bruger systemet - passwords
    - ls shadow -al
    - sudo cat shadow, til udskrivning

- Sårbarhed ved svage passwords
1. installer værktøjer john-the-ripper med kommandoen sudo apt install john
2. Her efter skal du downloade en wordlist kaldet rockyou med følgende kommando wget https://github.com/brannondorsey/naive-hashcat/releases/download/data/rockyou.txt.
3. Opret nu en bruger ved navn misternaiv og giv ham passwordet password
4. Hent nu det hashet kodeord for misternaiv med følgende kommando sudo cat /etc/shadow | grep misternaiv > passwordhash.txt
5. udskriv indholdet af filen passwordhash.txt, og bemærk hvilken krypterings algoritme der er brugt.
6. Eksekver nu kommandoen john -wordlist=rockyou.txt passwordhash.txt.
7. Kommandoen resulter i No password loaded. Dette er fordi john the ripper værktøjet ikke selv har kunne detekter hvilken algoritme det drejer sig om.
8. Eksekver nu kommandoen: john --format=crypt -wordlist=rockyou.txt passwordhash.txt.
format fortæller john the ripper hvilken type algoritme det drejer sig om
