# Resume 
## Philosophie of Science, Oxford Paperbacks
### What is sience 
#### Kapitel 1 
- Det første kapitel, handler om hvordan den "moderne" sience skal forståes, og om hvordan den er blevet udviklet igennem tiden. Udover udvikling, får man også en introduktion til nogen af de kendte philisofer. Udover det bliver man også introdukseret til falsifiable sicense. Som handler om at sicence godt kan bevises at være forkert, men stadig blive antaget som en form for philosofi.

#### Kapitel 2 
- Store dele af det andet kapitel handler om det at man stoler på introduktion og rutiner. Eksempelvis at man bliver ved at at stole på, at hvis man drejer et ret i en bil imod uret, så vil bilen dreje mod venstre istedet for at dreje til hørje. Udover det handler kapitel 2 også om chance og risiko for ting som kommer til at ske/er sket.

#### Kapitel 3
- Halvdelen af kapitel 3, handler om love omkring fysik, eksempelvis med loven omkring hvor meget en skygge kommer til at dække, hvis solen rammer et objekt fra en bestemt vinkel. Kapitelet handler også om irelevanse, eksempevis med hvorfor en mand ikke skal tage p-piller, fordi han fysisk ikke har en livmoder. I slutningen af kapitel 3, handler det om at sicens så kan svare på alt.